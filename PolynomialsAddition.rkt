#lang racket
; Authors:
; _Marco Alfaro Ramirez 
; _Jose Ramirez Gomez 
; _Cristian Nunez Tapia 
; Paradigmas de Programación
; Ciclo II 2018
; ====================================================
(require "Functions.rkt")
(define addition
  (lambda (_list _result)
    (cond
      ((< (length _list) 1)
       _result)
      (else
       (addition (cdr _list) (map + _result (car _list)))))))



(define pass+
  (lambda (_list)
    (addition (setEqualSizeToLists _list) (increaseSize '() (length (car (setEqualSizeToLists _list)))))))


(define p+
  (lambda _list
    (pass+ _list)))

(provide (all-defined-out))
